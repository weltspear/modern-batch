
import sys
# @macro @end
# @call <macro name>
# @include <directory>

class Macro:
    def __init__(self, macro_name):
        self.macro = []
        self.macro_name = macro_name

def preprocess(content):
    splited = content.splitlines()

    processed_lines = []
    macros = []
    current_macro = None
    current_line = 0

    for line in splited:
        if line.startswith("@macro"):
            if current_macro != None:
                print("Nested macros are not supported")
                print("Check line " + current_line)
                exit(1)
            
            macro_name = line.replace("@macro ", "")
            current_macro = Macro(macro_name)
            
        elif line.startswith("@end"):
            macros.append(current_macro)
            current_macro = None
            
        elif line.startswith("@call"):
            macro_name = line.replace("@call ", "")
            for macro in macros:
                if macro.macro_name == macro_name:
                    if current_macro != None:
                        current_macro.macro += macro.macro
                    else:
                        processed_lines += macro.macro

        elif line.startswith("@include"):
            included_file_name = line.replace("@include ", "")
            read = open(included_file_name, 'r')
            imacros, iprocessed_lines = preprocess(read.read())
            macros += imacros
            processed_lines += iprocessed_lines
            read.close()
            
        elif current_macro is not None:
            if (line != '\n'):
                current_macro.macro.append(line + '\n')
                
        else:
            if (line != '\n'):
                processed_lines.append(line + '\n')

    return macros, processed_lines

if __name__ == "__main__":
    read = open(sys.argv[1], 'r')
    write = open(sys.argv[2], "w")

    macros, processed_lines = preprocess(read.read())
    final = ""
    for line in processed_lines: 
        final += line
    
    write.write(final)
    read.close()
    write.close()
            
            

